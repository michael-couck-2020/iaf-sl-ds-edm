FROM devops.digital.belgium.be:1443/iaf-sl-shared-parent-image/master:latest

WORKDIR /app

COPY target/*.jar app.jar

CMD java ${JAVA_OPTS} -jar app.jar
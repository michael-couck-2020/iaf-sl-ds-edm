package be.bosa.dto.eco.iaf.sl.ds.edm.boundary.in;

import be.bosa.dto.eco.iaf.sl.common.exception.ApiException;
import be.bosa.dto.eco.iaf.sl.ds.edm.boundary.out.TemplateRepository;
import be.bosa.dto.eco.iaf.sl.ds.edm.entity.Template;
import be.bosa.dto.eco.iaf.sl.ds.edm.service.DocumentGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.api.GenerateDocumentApi;
import io.swagger.model.DocumentRequest;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

import io.swagger.api.UploadTemplateApi;

@Slf4j
@RestController
public class EDMBoundary implements UploadTemplateApi, GenerateDocumentApi {

    public static final String GENERATE_DOCUMENT = "/generateDocument";
    public static final String UPLOAD_TEMPLATE = "/uploadTemplate";

    private final DocumentGenerator documentGenerator;
    private final TemplateRepository templateRepository;

    @Autowired
    public EDMBoundary(final DocumentGenerator documentGenerator, final TemplateRepository templateRepository) {
        this.documentGenerator = documentGenerator;
        this.templateRepository = templateRepository;
    }

    @RequestMapping(value = GENERATE_DOCUMENT, method = RequestMethod.POST)
    public ResponseEntity<String> generateDocument(@Valid final DocumentRequest documentRequest, final String xBosaJWT) {
        try {
            String pdfString = documentGenerator.generateDocument(documentRequest);
            return ResponseEntity.ok(pdfString);
        } catch (final IOException | JRException e) {
            log.error("Exception generating PDF document : ", e);
            throw new ApiException(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = UPLOAD_TEMPLATE, method = RequestMethod.POST)
    public ResponseEntity<String> uploadTemplate(@Valid final String templateContent, final String xBosaJWT, final String templateName) {
        Template template = templateRepository.findTemplateByName(templateName);
        if (template != null) {
            templateRepository.delete(template);
        }
        Template newTemplate = Template.builder().templateName(templateName).templateContent(templateContent).build();
        templateRepository.save(newTemplate);
        return ResponseEntity.ok("Template uploaded");
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.empty();
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.empty();
    }

    @Override
    public Optional<String> getAcceptHeader() {
        return Optional.empty();
    }

}
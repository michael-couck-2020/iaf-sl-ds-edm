package be.bosa.dto.eco.iaf.sl.ds.edm.service;

import io.swagger.model.DocumentRequest;
import net.sf.jasperreports.engine.JRException;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author Michael Couck
 * @version 01.00
 * @since 04-03-2021
 */
@Service
public interface DocumentGenerator {

    String generateDocument(final DocumentRequest reportRequest) throws IOException, JRException;

}
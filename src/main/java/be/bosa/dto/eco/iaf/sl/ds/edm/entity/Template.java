package be.bosa.dto.eco.iaf.sl.ds.edm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Data
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "iaf_template")
@SuppressWarnings("JpaDataSourceORMInspection")
public class Template {

    @JsonIgnore
    @Id
    @Column(name = "templateId", nullable = false, unique = true, updatable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "template_sequence")
    @SequenceGenerator(name = "template_sequence", sequenceName = "template_sequence", allocationSize = 1)
    private Long templateId;

    @Column
    private String templateName;

    @Lob
    @Column
    private String templateContent;

    @JsonIgnore
    @Column(updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @JsonIgnore
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdateDate;

    @PrePersist
    public void prePersist() {
        long now = System.currentTimeMillis();
        creationDate = new Timestamp(now);
        lastUpdateDate = new Timestamp(now);
    }

    @PreUpdate
    public void preUpdate() {
        lastUpdateDate = new Timestamp(System.currentTimeMillis());
    }

}
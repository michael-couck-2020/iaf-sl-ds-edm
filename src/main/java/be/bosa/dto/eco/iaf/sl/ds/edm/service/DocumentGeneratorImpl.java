package be.bosa.dto.eco.iaf.sl.ds.edm.service;

import be.bosa.dto.eco.iaf.sl.ds.edm.boundary.out.TemplateRepository;
import be.bosa.dto.eco.iaf.sl.ds.edm.entity.Template;
import be.bosa.dto.eco.iaf.sl.ds.edm.tool.FILE;
import io.swagger.model.DocumentRequest;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.query.JsonQueryExecuterFactory;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Jasper : https://sourceforge.net/p/jasperreports/code/ci/6.16.0/tree/jasperreports/demo/samples/jsondatasource/
 *
 * @author Michael Couck
 * @version 01.00
 * @since 04-03-2021
 */
@Service
@SuppressWarnings({"ConstantConditions", "DuplicatedCode"})
public class DocumentGeneratorImpl implements DocumentGenerator {

    private final TemplateRepository templateRepository;
    private final Map<String, JasperReport> jasperReportMap;

    @Autowired
    public DocumentGeneratorImpl(final TemplateRepository templateRepository) {
        this.templateRepository = templateRepository;
        this.jasperReportMap = new HashMap<>();
    }

    @Override
    public synchronized String generateDocument(final DocumentRequest reportRequest) throws IOException, JRException {
        String templateName = reportRequest.getTemplate();
        JasperReport jasperReport = getJasperReport(templateName);

        if (jasperReport == null) {
            return "Report template not found : " + templateName;
        }

        InputStream inputStream = new ByteArrayInputStream(reportRequest.getData().getBytes(StandardCharsets.UTF_8));

        Map<String, Object> parameters = new HashMap<>();
        parameters.put(JsonQueryExecuterFactory.JSON_DATE_PATTERN, "yyyy-MM-dd");
        parameters.put(JsonQueryExecuterFactory.JSON_NUMBER_PATTERN, "#,##0.##");
        parameters.put(JsonQueryExecuterFactory.JSON_LOCALE, Locale.ENGLISH);
        parameters.put(JRParameter.REPORT_LOCALE, Locale.US);

        JRDataSource beanColDataSource = new JsonDataSource(inputStream);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);

        String fileName = reportRequest.getTemplate() + ".pdf";
        try {
            JasperExportManager.exportReportToPdfFile(jasperPrint, fileName);
            File pdfFile = FILE.findFile(new File("."), fileName);

            FileInputStream fileInputStream = new FileInputStream(pdfFile);
            byte[] pdfBytes = IOUtils.readFully(fileInputStream, (int) pdfFile.length());
            byte[] pdfBytesEncoded = Base64.getEncoder().encode(pdfBytes);
            return new String(pdfBytesEncoded);
        } finally {
            File pdfFile = FILE.findFile(new File("."), fileName);
            FileUtils.deleteQuietly(pdfFile);
        }
    }

    private JasperReport getJasperReport(final String templateName) throws JRException, IOException {
        JasperReport jasperReport = jasperReportMap.get(templateName);
        if (jasperReport == null) {
            JasperDesign jasperDesign = null;
            InputStream templateXmlInputStream;
            Template template = templateRepository.findTemplateByName(templateName);
            if (template != null) {
                templateXmlInputStream = new ByteArrayInputStream(template.getTemplateContent().getBytes());
                jasperDesign = JRXmlLoader.load(templateXmlInputStream);
            } else {
                File templateFile = FILE.findFile(new File("."), templateName);
                if (templateFile != null) {
                    jasperDesign = JRXmlLoader.load(templateFile);
                    byte[] pdfBytes = IOUtils.readFully(new FileInputStream(templateFile), (int) templateFile.length());
                    template = Template.builder().templateName(templateName).templateContent(new String(pdfBytes)).build();
                    templateRepository.save(template);
                }
            }
            if (jasperDesign == null) {
                return null;
            }
            jasperReport = JasperCompileManager.compileReport(jasperDesign);
            jasperReportMap.put(templateName, jasperReport);
        }
        return jasperReport;
    }

}
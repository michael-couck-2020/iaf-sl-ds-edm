package be.bosa.dto.eco.iaf.sl.ds.edm.boundary.out;

import be.bosa.dto.eco.iaf.sl.ds.edm.entity.Template;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Michael Couck
 * @version 01.00
 * @since 18-03-2021
 */
public interface TemplateRepository extends JpaRepository<Template, Long> {

    @Query(value = "select p from Template p where p.templateName < :templateName")
    Template findTemplateByName(final String templateName);

}
package be.bosa.eco.iaf.sl.ds.edm.boundary.in;

import be.bosa.dto.eco.iaf.sl.ds.edm.boundary.in.EDMBoundary;
import be.bosa.dto.eco.iaf.sl.ds.edm.boundary.out.TemplateRepository;
import be.bosa.dto.eco.iaf.sl.ds.edm.entity.Template;
import be.bosa.dto.eco.iaf.sl.ds.edm.service.DocumentGenerator;
import io.swagger.model.DocumentRequest;
import net.sf.jasperreports.engine.JRException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
public class EDMBoundaryTest {

    @Spy
    @InjectMocks
    private EDMBoundary edmBoundary;
    @Mock
    private DocumentGenerator documentGenerator;
    @Mock
    private TemplateRepository templateRepository;

    @Test
    public void generateReport() throws IOException, JRException {
        String pdfDataString = "PDF data in binary(i.e. bytes base 64)";
        Mockito.when(documentGenerator.generateDocument(Mockito.any(DocumentRequest.class))).thenReturn(pdfDataString);

        DocumentRequest documentRequest = new DocumentRequest();
        documentRequest.setData("{'any-data':'here'}");
        documentRequest.setTemplate("report-template-json.jrxml (any template identifier)");
        ResponseEntity<String> responseEntity = edmBoundary.generateDocument(documentRequest, null);
        String pdfDataResponseString = responseEntity.getBody();
        Assert.assertEquals(pdfDataString, pdfDataResponseString);
    }

    @Test
    public void uploadTemplate() {
        Template template = Mockito.mock(Template.class);
        Mockito.when(templateRepository.findTemplateByName(Mockito.anyString())).thenReturn(template);

        edmBoundary.uploadTemplate("xml template", "jwt", "template name");
        Mockito.verify(templateRepository, Mockito.times(1)).delete(template);
        Mockito.verify(templateRepository, Mockito.times(1)).save(Mockito.any(Template.class));
    }

}
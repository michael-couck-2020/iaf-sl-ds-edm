package be.bosa.eco.iaf.sl.ds.edm.boundary.in;

import be.bosa.common.services.jwt.JwtTokenHandlerService;
import be.bosa.common.services.jwt.model.AuthenticationTokenDetails;
import be.bosa.dto.eco.iaf.sl.ds.edm.Application;
import be.bosa.dto.eco.iaf.sl.ds.edm.boundary.in.EDMBoundary;
import be.bosa.dto.eco.iaf.sl.ds.edm.tool.FILE;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.model.DocumentRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

@Slf4j
@ActiveProfiles({"test", "noauth"})
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EDMBoundaryIT {

    @LocalServerPort
    private int port;

    @Value("${server.servlet.context-path:/iaf/sl/ds/edm/1.0}")
    private String context;

    @Autowired
    private RestTemplate restTemplate;

    private final String nrn = "71052725793";
    // Must mock this, the alternative causes insanity
    @MockBean
    private JwtTokenHandlerService jwtTokenHandlerService;

    @Before
    public void before() throws IOException {
        String token = getJwt();
        AuthenticationTokenDetails authenticationTokenDetails = new AuthenticationTokenDetails();
        authenticationTokenDetails.setSubAndFillNrnAndCbe(nrn);
        when(jwtTokenHandlerService.parseToken(token)).thenReturn(authenticationTokenDetails);
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    public void generateReport() throws IOException {
        File jsonDataFile = FILE.findFile(new File("."), "report-data-json.json");
        String jsonData = FileUtils.readFileToString(jsonDataFile, Charset.defaultCharset());
        String edmEndpointUrl = "http://localhost:" + port + "" + context + "" + EDMBoundary.GENERATE_DOCUMENT;

        DocumentRequest documentRequest = new DocumentRequest();
        documentRequest.setData(jsonData);
        documentRequest.setTemplate("report-template-json.jrxml");

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-bosa-JWT", getJwt());
        HttpEntity<DocumentRequest> requestHttpEntity = new HttpEntity<>(documentRequest, headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange(edmEndpointUrl, HttpMethod.POST, requestHttpEntity, String.class);
        String pdfData = responseEntity.getBody();
        Assert.assertTrue(StringUtils.isNotEmpty(pdfData));
        byte[] pdfBytesDecoded = Base64.getDecoder().decode(pdfData.getBytes());

        File outputReport = new File("target/report-template-json.pdf");
        FileOutputStream fileOutputStream = new FileOutputStream(outputReport);
        IOUtils.write(pdfBytesDecoded, fileOutputStream);
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    public void uploadTemplate() throws IOException {
        String templateName = "report-template-json.jrxml";
        File templateFile = FILE.findFile(new File("."), templateName);
        String templateContent = FileUtils.readFileToString(templateFile, Charset.defaultCharset());
        String edmEndpointUrl = "http://localhost:" + port + "" + context + "" + EDMBoundary.UPLOAD_TEMPLATE;

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(edmEndpointUrl).queryParam("templateName",templateName).build();

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-bosa-JWT", getJwt());
        HttpEntity<String> requestHttpEntity = new HttpEntity<>(templateContent, headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, requestHttpEntity, String.class);

        String response = responseEntity.getBody();
        Assert.assertTrue(StringUtils.isNotEmpty(response));
        Assert.assertEquals("Template uploaded", response);
    }

    private String getJwt() {
        String jwt = Jwts.builder()
                .setSubject(nrn)
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60))
                .signWith(SignatureAlgorithm.HS512, "secret")
                .compact();
        log.debug("Jwt : {}", jwt);
        return jwt;
    }

}
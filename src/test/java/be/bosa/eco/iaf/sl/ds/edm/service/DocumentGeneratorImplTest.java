package be.bosa.eco.iaf.sl.ds.edm.service;

import be.bosa.dto.eco.iaf.sl.ds.edm.boundary.out.TemplateRepository;
import be.bosa.dto.eco.iaf.sl.ds.edm.service.DocumentGeneratorImpl;
import be.bosa.dto.eco.iaf.sl.ds.edm.tool.FILE;
import io.swagger.model.DocumentRequest;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.class)
public class DocumentGeneratorImplTest {

    @Mock
    private TemplateRepository templateRepository;

    @Spy
    @InjectMocks
    private DocumentGeneratorImpl documentGenerator;

    @Test
    @SuppressWarnings("ConstantConditions")
    public void generateDocument() throws IOException, JRException {
        File file = FILE.findFile(new File("."), "report-data-json.json");
        String json = FileUtils.readFileToString(file, Charset.defaultCharset());
        DocumentRequest reportRequest = new DocumentRequest();
        reportRequest.setData(json);
        reportRequest.setTemplate("report-template-json.jrxml");
        String pdfData = documentGenerator.generateDocument(reportRequest);
        Assert.assertTrue("Should be some PDF data", pdfData.length() > 0);

        // To visualise the result
        byte[] pdfBytesDecoded = Base64.getDecoder().decode(pdfData.getBytes());
        File outputReport = new File("target/report-template-json.pdf");
        FileOutputStream fileOutputStream = new FileOutputStream(outputReport);
        IOUtils.write(pdfBytesDecoded, fileOutputStream);
    }

}